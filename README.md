# lft-blog

## Build and Run local dockerfile (built-in SQLite)
### Build
```shell
docker build -t lgt-blog-local .
```
### Run
```shell
docker run -d -p 2368:2368 lft-blog-local
```

## Stand up Ghost blog and MySQL with docker-compose
```shell 
docker-compose up
```

## Usage
- Landing page of the Ghost blog: `localhost:2368`
- Admin page of the Ghost blog: `localhost:2368/ghost`